<?php

/*
	Template Name: Home
*/

get_header(); ?>

    <?php if (post_password_required() ) : ?>

		<?php get_template_part('template-parts/homepage/login'); ?>

	<?php else: ?>

		<?php get_template_part('template-parts/homepage/intro'); ?>
		<?php get_template_part('template-parts/homepage/filter'); ?>

		<section class="documents grid">
			<?php get_template_part('template-parts/homepage/documents-categories'); ?>
			<?php get_template_part('template-parts/homepage/documents-list'); ?>
		</section>
	
	<?php endif; ?>

<?php get_footer(); ?>