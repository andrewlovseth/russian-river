(function ($, window, document, undefined) {
    function selectFilters() {
        let textFilterValue = $('#rr-documents-filter').val().toLowerCase();
        let subject1Value = $('#subject-1').val();
        let subject2Value = $('#subject-2').val();
        let authorsValue = $('#authors').val();
        let yearValue = $('#years').val();

        let queryString = '.file';
        let textSearch = '';

        function contains(selector, text) {
            var elements = document.querySelectorAll(selector);
            return Array.prototype.filter.call(elements, function (element) {
                return RegExp(text).test(element.textContent.toLowerCase());
            });
        }

        if (subject1Value !== '') {
            queryString += `[data-first-subject="${subject1Value}"]`;
        }

        if (subject2Value !== '') {
            queryString += `[data-second-subject="${subject2Value}"]`;
        }

        if (authorsValue !== '') {
            queryString += `[data-author*="${authorsValue}"]`;
        }

        if (yearValue !== '') {
            queryString += `[data-year="${yearValue}"]`;
        }

        if (textFilterValue !== '') {
            textSearch = textFilterValue;
        }

        if (queryString !== '.file' || textFilterValue !== '') {
            $('.section-header, .file').addClass('hide');

            let filteredFiles;

            if (textSearch) {
                console.log(textSearch);

                filteredFiles = contains(queryString, textSearch);
            } else {
                filteredFiles = document.querySelectorAll(queryString);
            }

            filteredFiles.forEach((file) => {
                file.classList.remove('hide');
            });
        } else {
            $('.section-header, .file').removeClass('hide');
        }
    }

    $(document).ready(function ($) {
        // SEARCH FILTER
        $('#rr-documents-filter').on('keyup', function () {
            selectFilters();
        });

        // DROPDOWN FILTERS
        $('#subject-1, #subject-2, #authors, #years').on('change', function () {
            selectFilters();
        });

        // CLEAR BTN
        $('.clear-btn').on('click', function () {
            $('#rr-documents-filter').val('');

            $('.section-header, .file').removeClass('hide');

            $('#subject-1, #subject-2, #authors, #years').prop(
                'selectedIndex',
                0
            );
            selectFilters();

            return false;
        });

        // HOW TO OPEN
        $('.js-how-to-trigger').click(function () {
            $('.how-to-modal').fadeIn(200);
            $('body').addClass('how-to-overlay-open');

            return false;
        });

        // HOW TO CLOSE
        $('.js-how-to-close').click(function () {
            $('.how-to-modal').fadeOut(200);
            $('body').removeClass('how-to-overlay-open');

            return false;
        });

        $('body').on('keyup', '#password-field', function (e) {
            let passwordField = $('.field.password');
            let submitBtn = $('#password-submit');
            let inputValue = $('#password-field').val();

            if (inputValue.length >= 5) {
                submitBtn.prop('disabled', false);
                passwordField.addClass('filled');
            } else {
                submitBtn.prop('disabled', true);
                passwordField.removeClass('filled');
            }
        });

        $('.eyeball').on('click', function () {
            $(this).toggleClass('active');

            let passwordField = $('#password-field');
            if (passwordField.attr('type') === 'password') {
                passwordField.attr('type', 'text');
            } else {
                passwordField.attr('type', 'password');
            }
        });
    });

    //  HOW TO CLOSE - Click anywhere else
    $(document).mouseup(function (e) {
        var modal = $('.how-to-modal .info');

        // if the target of the click isn't the container nor a descendant of the container
        if (!modal.is(e.target) && modal.has(e.target).length === 0) {
            $('.how-to-modal').fadeOut(200);
            $('body').removeClass('how-to-overlay-open');
        }
    });

    // HOW TO CLOSE - Escape
    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('.how-to-modal').fadeOut(200);
            $('body').removeClass('how-to-overlay-open');
        }
    });
})(jQuery, window, document);
