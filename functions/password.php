<?php

// Custom password protected form
function esa_password_form() {
	$o = '
		<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
			<div class="field password">
				<input name="post_password" id="password-field" type="password" size="20" maxlength="20" placeholder="Enter Password" />
				<span class="eyeball">
					<svg width="24" height="17" viewBox="0 0 24 17" fill="none" xmlns="http://www.w3.org/2000/svg">
						<g opacity="0.2">
							<path d="M22.9947 8.73132C21.1284 4.0213 16.5961 0.910912 11.5307 0.999781C6.46517 0.910912 1.93288 4.0213 0.0666512 8.73132C-0.0222171 8.90906 -0.0222171 9.08679 0.0666512 9.3534C1.93288 13.9745 6.46517 17.0849 11.5307 16.9961C16.5961 17.0849 21.1284 13.9745 22.9947 9.3534C23.0835 9.08679 23.0835 8.90906 22.9947 8.73132ZM11.5307 13.4413C9.04235 13.4413 7.08724 11.4862 7.08724 8.99792C7.08724 6.50961 9.04235 4.55451 11.5307 4.55451C14.019 4.55451 15.9741 6.50961 15.9741 8.99792C15.9741 11.4862 14.019 13.4413 11.5307 13.4413Z" fill="black"/>
							<path d="M11.5308 11.664C13.0032 11.664 14.1968 10.4703 14.1968 8.99793C14.1968 7.52551 13.0032 6.33188 11.5308 6.33188C10.0584 6.33188 8.86475 7.52551 8.86475 8.99793C8.86475 10.4703 10.0584 11.664 11.5308 11.664Z" fill="black"/>
						</g>
					</svg>
				</span>
				<input type="hidden" name="_wp_http_referer" value="'.get_permalink().'" />
			</div>

			<div class="submit-btn">
				<input type="submit" id="password-submit" name="Submit" value="' . esc_attr__( "Login" ) . '" disabled />
				<span class="caret">
					<svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M5.98374 7.93496L0.390244 13.7886C0.130081 14.0488 0 14.3089 0 14.6992C0 15.0894 0.130081 15.3496 0.390244 15.6098C0.910569 16.1301 1.69106 16.1301 2.21138 15.6098L8.71545 8.84553C8.97561 8.58537 9.10569 8.3252 9.10569 7.93496C9.10569 7.6748 8.97561 7.28455 8.71545 7.15447L2.21138 0.390244C1.95122 0.130081 1.69106 0 1.30081 0C1.04065 0 0.650406 0.130081 0.390244 0.390244C-0.130081 0.910569 -0.130081 1.69106 0.390244 2.21138L5.98374 7.93496Z" fill="#FFFFFF"/>
					</svg>
				</span>
			</div>
		</form>';
	return $o;
}
add_filter('the_password_form', 'esa_password_form');



/**
 * Add a message to the password form.
 *
 * @wp-hook the_password_form
 * @param   string $form
 * @return  string
 */
function esa_password_error( $form ) {
    // No cookie, the user has not sent anything until now.
    if ( ! isset ( $_COOKIE[ 'wp-postpass_' . COOKIEHASH ] ) )
        return $form;

    // Translate and escape.
    $msg = esc_html__( 'Sorry, your password is incorrect.', 'esa' );

    // We have a cookie, but it doesn’t match the password.
    $msg = "<div class='error-message'><p>$msg</p></div>";

    return $form . $msg;
}
add_filter( 'the_password_form', 'esa_password_error' );