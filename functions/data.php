<?php

function esa_set_data_transients() {
    $frontpage_id = get_option('page_on_front');
    $google_sheet = get_field('google_sheet', $frontpage_id);
    $spreadsheet_id = $google_sheet['sheet_id'];
    $document_tab_id = $google_sheet['documents_tab_id'];
    $categories_tab_id = $google_sheet['categories_tab_id'];
    $subject_1_tab_id = $google_sheet['subject_1_tab_id'];
    $subject_2_tab_id = $google_sheet['subject_2_tab_id'];
    
    // DOCUMENT LIST
    $document_url = "https://spreadsheets.google.com/feeds/list/" . $spreadsheet_id . "/" . $document_tab_id . "/public/values?alt=json";
    
    if(false == ($documents_list = get_transient('documents_list'))) {
        $documents_list = [];
        $api_repsonse = wp_remote_get($document_url);
        $api_repsonse_body = wp_remote_retrieve_body($api_repsonse);
        $feed = json_decode($api_repsonse_body, true);
        $entries = $feed['feed']['entry'];
    
        foreach($entries as $entry) {
            $documents_list[] = array(
                'title' => $entry['gsx$title']['$t'],
                'author' => $entry['gsx$author']['$t'],
                'date' => $entry['gsx$date']['$t'],
                'year' => $entry['gsx$year']['$t'],
                'subject_1' => $entry['gsx$subject-1']['$t'],
                'subject_2' => $entry['gsx$subject-2']['$t'],
                'study_area' => $entry['gsx$study-area']['$t'],
                'description' => $entry['gsx$description']['$t'],
                'filename' => $entry['gsx$file-name']['$t'],
                'ext' => $entry['gsx$extension']['$t'],
                'date_uploaded' => $entry['gsx$date-uploaded']['$t'],
                'status' => $entry['gsx$status']['$t']
            );
        }
    
        set_transient('documents_list', $documents_list, MINUTE_IN_SECONDS);    
    } 
    
    
    // CATEGORIES LIST
    $categories_url = "https://spreadsheets.google.com/feeds/list/" . $spreadsheet_id . "/" . $categories_tab_id . "/public/values?alt=json";
    
    if(false == ($categories_list = get_transient('categories_list'))) {
        $categories_list = [];
        $api_repsonse = wp_remote_get($categories_url);
        $api_repsonse_body = wp_remote_retrieve_body($api_repsonse);
        $feed = json_decode($api_repsonse_body, true);
        $entries = $feed['feed']['entry'];
    
        foreach($entries as $entry) {
            $categories_list[] = array(
                'title' => $entry['gsx$title']['$t']
            );
        }
    
        set_transient('categories_list', $categories_list, MINUTE_IN_SECONDS);    
    } 


    // SUBJECT 1 LIST
    $subject_1_url = "https://spreadsheets.google.com/feeds/list/" . $spreadsheet_id . "/" . $subject_1_tab_id . "/public/values?alt=json";
    
    if(false == ($subject_1_list = get_transient('subject_1_list'))) {
        $subject_1_list = [];
        $api_repsonse = wp_remote_get($subject_1_url);
        $api_repsonse_body = wp_remote_retrieve_body($api_repsonse);
        $feed = json_decode($api_repsonse_body, true);
        $entries = $feed['feed']['entry'];
    
        foreach($entries as $entry) {
            $subject_1_list[] = array(
                'title' => $entry['gsx$title']['$t']
            );
        }
    
        set_transient('subject_1_list', $subject_1_list, MINUTE_IN_SECONDS);    
    } 

    // SUBJECT 2 LIST
    $subject_2_url = "https://spreadsheets.google.com/feeds/list/" . $spreadsheet_id . "/" . $subject_2_tab_id . "/public/values?alt=json";
    
    if(false == ($subject_2_list = get_transient('subject_2_list'))) {
        $subject_2_list = [];
        $api_repsonse = wp_remote_get($subject_2_url);
        $api_repsonse_body = wp_remote_retrieve_body($api_repsonse);
        $feed = json_decode($api_repsonse_body, true);
        $entries = $feed['feed']['entry'];
    
        foreach($entries as $entry) {
            $subject_2_list[] = array(
                'title' => $entry['gsx$title']['$t']
            );
        }
    
        set_transient('subject_2_list', $subject_2_list, MINUTE_IN_SECONDS);    
    } 

}

add_action('init', 'esa_set_data_transients');
