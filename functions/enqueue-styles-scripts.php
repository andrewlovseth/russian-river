<?php

/*
	Enqueue Styles & Scripts
*/

function esa_enqueue_child_styles_and_scripts() {
    // Add child theme css and js
    wp_enqueue_style( 'russian-river-css', get_stylesheet_directory_uri() . '/russian-river.css', '' );
    wp_enqueue_script( 'russian-river-js', get_stylesheet_directory_uri() . '/js/russian-river.js', array( 'jquery.3.5.1' ));
}

add_action( 'wp_enqueue_scripts', 'esa_enqueue_child_styles_and_scripts', 11 );