<?php

$args = wp_parse_args($args);
if(!empty($args)) {
    $spreadsheet_id = $args['spreadsheet_id']; 
    $documents_tab_id = $args['documents_tab_id']; 
    $statuses = $args['statuses']; 
}

$doc_transient_name = 'rr_documents_list';
$documents = esa_get_transient_data($doc_transient_name, $spreadsheet_id, HOUR_IN_SECONDS, $documents_tab_id);
$authors_array = [];

foreach($documents as $document) {
    $author = $document[1];
    $status = $document[11];

    if(in_array($status, $statuses)) {
        if(strpos($author, ',') !== false) {
            $multi_authors = preg_split ("/\,/", $author);
            foreach($multi_authors as $multi_author) {
                $multi_author = ltrim($multi_author);
                array_push($authors_array, $multi_author);
            } 
        } else {
            array_push($authors_array, $author);
        }
    }
}

$authors = custom_sort($authors_array);

if(!empty($authors)): ?>

    <div class="dropdown authors">
        <label for="authors">Filter by Author</label>
        <select name="authors" id="authors">
            <option value="">-- Select --</option>

            <?php 
                foreach ($authors as $entry): 
                    $title = $entry;
            ?>                    
                <option value="<?php echo $title; ?>"><?php echo $title; ?></option>
            <?php endforeach; ?>			
        </select>
    </div>

<?php endif; ?>