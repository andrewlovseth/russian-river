<div class="how-to">
    
    <a href="#" class="trigger js-how-to-trigger">
        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-question.svg" alt="How to" />
        <span>How to use this site?</span>
    </a>

    <div class="how-to-modal">
        <div class="overlay">
            <div class="overlay-wrapper">

                <div class="info">
                    <div class="close">
                        <a href="#" class="js-how-to-close"></a>
                    </div>

                    <div class="copy extended">
                        <?php echo get_field('how_to'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>