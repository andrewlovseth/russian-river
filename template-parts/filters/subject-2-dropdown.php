<?php

$args = wp_parse_args($args);

if(!empty($args)) {
    $spreadsheet_id = $args['spreadsheet_id']; 
    $subject_2_tab_id = $args['subject_2_tab_id']; 
}

$subject_2_transient = 'rr_subject_2_list';
$subject_2_list = esa_get_transient_data($subject_2_transient, $spreadsheet_id, HOUR_IN_SECONDS, $subject_2_tab_id);


if(!empty($subject_2_list)): ?>

    <div class="dropdown subject-1">
        <label for="subject-2">Filter by Subject 2</label>
        <select name="subject-2" id="subject-2">
            <option value="">-- Select --</option>

            <?php 
                foreach ($subject_2_list as $entry): 
                    $title = $entry[0];
            ?>                    
                <option value="<?php echo $title; ?>"><?php echo $title; ?></option>
            <?php endforeach; ?>			
        </select>
    </div>

<?php endif; ?>