<?php

$args = wp_parse_args($args);

if(!empty($args)) {
    $spreadsheet_id = $args['spreadsheet_id']; 
    $documents_tab_id = $args['documents_tab_id']; 
    $statuses = $args['statuses']; 

}

$doc_transient_name = 'rr_documents_list';
$documents = esa_get_transient_data($doc_transient_name, $spreadsheet_id, HOUR_IN_SECONDS, $documents_tab_id);

$years_array = [];

foreach($documents as $document) {
    $status = $document[11];

    if(in_array($status, $statuses)) {
        array_push($years_array, $document[3]);
    }
}

$years = custom_reverse_sort($years_array);

if(!empty($years)): ?>

    <div class="dropdown authors">
        <label for="years">Filter by Year</label>
        <select name="years" id="years">
            <option value="">-- Select --</option>

            <?php 
                foreach ($years as $entry): 
                    $title = $entry;
                    $slug = sanitize_title_with_dashes($title);
            ?>                    
                <option value="<?php echo $slug; ?>"><?php echo $title; ?></option>
            <?php endforeach; ?>			
        </select>
    </div>

<?php endif; ?>