<div class="filter-input">
    <label for="site-search">Filter by Keyword</label>
    <input type="search" placeholder="Search..." id="rr-documents-filter" name="site-search"
            aria-label="Filter documents by Keyword">

    <button>Filter</button>
</div>