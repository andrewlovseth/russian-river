<?php

$args = wp_parse_args($args);

if(!empty($args)) {
    $spreadsheet_id = $args['spreadsheet_id']; 
    $subject_1_tab_id = $args['subject_1_tab_id']; 
}

$subject_1_transient = 'rr_subject_1_list';
$subject_1_list = esa_get_transient_data($subject_1_transient, $spreadsheet_id, HOUR_IN_SECONDS, $subject_1_tab_id);

if(!empty($subject_1_list)): ?>

    <div class="dropdown subject-1">
        <label for="subject-1">Filter by Subject 1</label>
        <select name="subject-1" id="subject-1">
            <option value="">-- Select --</option>

            <?php 
                foreach ($subject_1_list as $entry): 
                    $title = $entry[0];
            ?>                    
                <option value="<?php echo $title; ?>"><?php echo $title; ?></option>
            <?php endforeach; ?>			
        </select>
    </div>

<?php endif; ?>