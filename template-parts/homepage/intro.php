<?php if(get_field('intro_copy')): ?>

    <section class="intro grid">
        <div class="copy p1">
            <?php echo get_field('intro_copy'); ?>

            <?php if(get_field('contact_info')): ?>
                <div class="contact">
                    <?php echo get_field('contact_info'); ?>
                </div>
            <?php endif; ?>
        </div>        
    </section>

<?php endif; ?>

<?php get_template_part('template-parts/homepage/log'); ?>