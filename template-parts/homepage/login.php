<section class="login grid">

    <div class="page-header">
        <h2>Please login below to<br/>access our Project Library.</h2>
    </div>

    <div class="login-form">
        <?php echo get_the_password_form(); ?>
    </div>
</section>