<?php 

$google_sheet = get_field('google_sheet');
$spreadsheet_id = $google_sheet['sheet_id'];
$documents_tab_id = $google_sheet['documents_tab_id'];
$categories_tab_id = $google_sheet['categories_tab_id'];
$status_field = $google_sheet['status'];
$statuses = array_map('trim', explode(',', $status_field));
$base_url = get_field('document_base_url');

$doc_transient_name = 'rr_documents_list';
if(get_transient($doc_transient_name)) {
    $documents = get_transient($doc_transient_name);
} else {
    $documents = esa_get_sheet_data($doc_transient_name, $spreadsheet_id, HOUR_IN_SECONDS, $documents_tab_id);
}

$cat_transient_name = 'rr_categories_list';
if(get_transient($cat_transient_name)) {
    $categories = get_transient($cat_transient_name);
} else {
    $categories = esa_get_sheet_data($cat_transient_name, $spreadsheet_id, HOUR_IN_SECONDS, $categories_tab_id);
}

if(!empty($categories)): ?>

	<div class="documents-list">

		<?php 
			foreach ($categories as $category): 
				$cat_title = $category[0];
				$cat_slug = sanitize_title_with_dashes($cat_title);
				$files = array_filter($documents, function ($document) use ($cat_title) {
					return ($document[6] == $cat_title); }
				);

				if(!empty($files)): ?>
			
					<div id="<?php echo $cat_slug; ?>" class="category">
						<div class="section-header">
							<h3><?php echo $cat_title; ?></h3>
						</div>

						

						<?php
							foreach ($files as $file): 
								$document_title = $file[0];
								$author = $file[1];
								$date = $file[2];
								$year = $file[3];
								$subject_1 = $file[4];
								$subject_2 = $file[5];
								$description = $file[7];
								$filename = $file[8];
								$ext = $file[9];
								$date_uploaded = $file[10];
								$status = $file[11];

								$className = 'file';
						?>

							<?php if(in_array($status, $statuses)): ?>

								<div
									data-first-subject="<?php echo $subject_1; ?>" 
									data-second-subject="<?php echo $subject_2; ?>" 
									data-author="<?php echo $author; ?>"
									data-year="<?php echo $year; ?>" 
									class="<?php echo esc_attr($className); ?>">
									<div class="file-flex">
										<div class="info">
											<div class="title">
												<?php if($file !== ''): ?>
													<a href="<?php echo $base_url; ?><?php echo $filename; ?>.<?php echo $ext; ?>" rel="external"><?php echo $document_title; ?></a>
												<?php else: ?>
													<?php echo $document_title; ?>
												<?php endif; ?>											
											</div>

											<div class="description">
												<?php echo $description; ?>
											</div>

											<div class="details">
												<div class="author"><span>Author:</span> <?php echo $author; ?></div>
												<div class="date"><span>Year:</span> <?php echo $year; ?></div>
											</div>

											<div class="meta">
												<div class="subject-1"><span>Subject 1:</span> <?php echo $subject_1; ?></div>
												<div class="subject-2"><span>Subject 2:</span> <?php echo $subject_2; ?></div>
												<div class="cat"><span>Study Area:</span> <?php echo $cat_title; ?></div>
											</div>
										</div>

										<div class="download">
											<?php if($file !== ''): ?>
												<a href="<?php echo $base_url; ?><?php echo $filename; ?>.<?php echo $ext; ?>" rel="external">Download (<?php echo $ext; ?>)</a>
											<?php endif; ?>	
										</div>
									</div>								
								</div>

							<?php endif; ?>

						<?php endforeach; ?>

					</div>

				<?php endif; ?>			
		
		<?php endforeach; ?>

	</div>

<?php endif; ?>