<?php

$google_sheet = get_field('google_sheet');
$spreadsheet_id = $google_sheet['sheet_id'];
$documents_tab_id = $google_sheet['documents_tab_id'];

$initial_upload_date = get_field('google_sheet_initial_upload_date');
$log_items = get_field('google_sheet_log_items');
$base_url = get_field('document_base_url');

$status_field = get_field('google_sheet_status');
$statuses = array_map('trim', explode(',', $status_field));

$doc_transient_name = 'rr_documents_list';
if(get_transient($doc_transient_name)) {
    $documents = get_transient($doc_transient_name);
} else {
    $documents = esa_get_sheet_data($doc_transient_name, $spreadsheet_id, HOUR_IN_SECONDS, $documents_tab_id);
}

$sorted_documents = array_filter($documents, function ($var) {
    return ($var[10] !== $initial_upload_date);
});



$keys = array_column($sorted_documents, 10);
array_multisort($keys, SORT_DESC, $sorted_documents);

foreach($sorted_documents as $document) {
    $status = $document[11];
    if(in_array($status, $statuses)) {
        $recent_documents[] = $document;
    }
}


if($recent_documents):

?>

    <section class="log grid">
        <div class="info">
            <div class="headline">
                <h5>Recent Document Additions</h5>
            </div>

            <ul>
                <?php $i = 0; foreach($recent_documents as $document): ?>

                    <li>
                        <em><?php echo $document[10]; ?>:</em>
                        <a href="<?php echo $base_url; ?><?php echo $document[8]; ?>.<?php echo $document[9]; ?>" rel="external"><?php echo $document[0]; ?></a>
                    </li>

                <?php if (++$i == 3) break; endforeach; ?>
            </ul>
        </div>

    </section>

<?php endif; ?>