<?php 

	$google_sheet = get_field('google_sheet');
	$spreadsheet_id = $google_sheet['sheet_id'];
	$categories_tab_id = $google_sheet['categories_tab_id'];

	$cat_transient_name = 'rr_categories_list';
	if(get_transient($cat_transient_name)) {
		$categories = get_transient($cat_transient_name);
	} else {
		$categories = esa_get_sheet_data($cat_transient_name, $spreadsheet_id, HOUR_IN_SECONDS, $categories_tab_id);
	}

	if(!empty($categories)):

?>

	<div class="documents-categories">
		<div class="sticky-wrapper">
		
			<div class="section-header">
				<h4>Study Areas</h4>
			</div>

			<div class="categories-list">
				<h5>Jump to:</h5>

				<ul>
					<?php 
						foreach ($categories as $category): 
							$title = $category[0];
							$slug = sanitize_title_with_dashes($title);
					?>

						<li><a href="#<?php echo $slug; ?>" data-cat="<?php echo $slug; ?>"><?php echo $title; ?></a></li>

					<?php endforeach; ?>			
				</ul>

			</div>			
		</div>
	</div>

<?php endif; ?>