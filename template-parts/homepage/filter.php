<?php 

    $google_sheet = get_field('google_sheet');
    $spreadsheet_id = $google_sheet['sheet_id'];
    $documents_tab_id = $google_sheet['documents_tab_id'];
    $subject_1_tab_id = $google_sheet['subject_1_tab_id'];
    $subject_2_tab_id = $google_sheet['subject_2_tab_id'];
    $status_field = $google_sheet['status'];
    $statuses = array_map('trim', explode(',', $status_field));
?>

<section class="filter grid">

    <?php get_template_part('template-parts/filters/how-to'); ?>

	<form>
        <?php get_template_part('template-parts/filters/keyword'); ?>

        <div class="filter-dropdowns">
            <?php
                $args = [
                    'spreadsheet_id' => $spreadsheet_id,
                    'subject_1_tab_id' => $subject_1_tab_id,
                    'statuses' => $statuses,
                ];
                get_template_part('template-parts/filters/subject-1-dropdown', null, $args);
            ?>

            <?php
                $args = [
                    'spreadsheet_id' => $spreadsheet_id,
                    'subject_2_tab_id' => $subject_2_tab_id,
                    'statuses' => $statuses,
                ];
                get_template_part('template-parts/filters/subject-2-dropdown', null, $args);
            ?>

            <?php
                $args = [
                    'spreadsheet_id' => $spreadsheet_id,
                    'documents_tab_id' => $documents_tab_id,
                    'statuses' => $statuses,
                ];
                get_template_part('template-parts/filters/authors', null, $args);
            ?>

            <?php
                $args = [
                    'spreadsheet_id' => $spreadsheet_id,
                    'documents_tab_id' => $documents_tab_id,
                    'statuses' => $statuses,
                ];
                get_template_part('template-parts/filters/years', null, $args);
            ?>
        </div>

        <?php get_template_part('template-parts/filters/clear'); ?>
	</form>
	
</section>