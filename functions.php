<?php

require_once( plugin_dir_path( __FILE__ ) . '/functions/enqueue-styles-scripts.php');

//require_once( plugin_dir_path( __FILE__ ) . '/functions/data.php');

require_once( plugin_dir_path( __FILE__ ) . '/functions/password.php');

function custom_sort( $array ) {
    $unique_array = array_unique($array);
    sort($unique_array);
    return $unique_array;
}

function custom_reverse_sort( $array ) {
    $unique_array = array_unique($array);
    rsort($unique_array);
    return $unique_array;
}


function esa_get_transient_data($transient_name, $spreadsheet_id, $expire, $tab_id) {
    if(get_transient($transient_name)) {
        $documents = get_transient($transient_name);
    } else {
        $documents = esa_get_sheet_data($transient_name, $spreadsheet_id, $expire, $tab_id);
    }
    return $documents;
}